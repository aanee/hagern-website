<?php

function send_mail() {

// if the url field is empty
	if(isset($_POST['url']) && $_POST['url'] == ''){
		// put your email address here
		$youremail = 'info@hagern.se';
		// prepare a "pretty" version of the message
		// Important: if you added any form fields to the HTML, you will need to add them here also
		$body = "This is the form that was just submitted:
		Name:  $_POST[name]
		E-Mail: $_POST[email]
		Message: $_POST[message]";
		

		// Use the submitters email if they supplied one
		// (and it isn't trying to hack your form).
		// Otherwise send from your email address.
		if( $_POST['email'] && !preg_match( "/[\r\n]/", $_POST['email']) ) {
		  $headers = "From: $_POST[email]";
		} 
		 
		else {
	  		$headers = "From: $youremail";
		}
		
		// finally, send the message
		$mail_status = mail($youremail, 'Contact Form', $body, $headers);
		

		if ($mail_status) { 
		
		    ?>
			<script language="javascript" type="text/javascript">
				alert('Thank you for the message. We will contact you shortly.');
				window.location = 'index.html';
			</script>
		    <?php
		    
	    }
	    else {
	    	?>
			<script language="javascript" type="text/javascript">
			alert('Failed to send the message, try again!');
			window.location = 'index.html';
			</script>
	    	<?php
		}
	}
}

// Checks if form has been submitted
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	
	if(!$_POST['name'] || !$_POST['message'] || !$_POST['email']) {
		header('Location: index.html#kontakta');
	}
	
    function post_captcha($user_response) {
		
        $url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => '6LdVsJYUAAAAAAQwOG_aZdNJk6aEbYkLD7OesDrs',
			'response' => $_POST["g-recaptcha-response"]
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success=json_decode($verify);

        return $captcha_success;
    }

    // Call the function post_captcha
    $res = post_captcha($_POST['g-recaptcha-response']);



    if ($res->success==false) {
        // What happens when the CAPTCHA wasn't checked
        echo '<p>Please go back and make sure you check the security CAPTCHA box.</p><br>';
    } else {
    	send_mail();
    } 
} else {
	// If the form has not been sent.
	header("Location: index.html#kontakta");
}


	
// otherwise, let the spammer think that they got their message through
/*
?>


	<script language="javascript" type="text/javascript">
		alert('Thank you for the message. We will contact you shortly.');
		window.location = 'index.html';
	</script>


<?php
*/
?>